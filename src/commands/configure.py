import json
import os

CONFIG_DIR = os.path.normpath(
    os.path.join(os.path.dirname(__file__), '..', '..', 'config', 'player')
)
CONFIG_PATH = os.path.join(CONFIG_DIR, 'player')

if not os.path.exists(CONFIG_DIR):
    os.makedirs(CONFIG_DIR)


def prompt_configure():
    data = read_config()

    while True:
        devel_mode = raw_input(
            u'Development mode (y/n, default - previous value: {}): '.format(
                'y' if data['devel_mode'] else 'n'
            ).encode('utf-8')
        ).lower()

        if devel_mode in ('y', 'n', ''):
            break

        print 'Invalid option, must be y or n'

    user_addr = raw_input(
        u'Input your address (default - previous value: {}): '.format(
            data['user_addr']
        ).encode('utf-8')
    )
    username = raw_input(
        u'Input your username (default - previous value: {}): '.format(
            data['username']
        ).encode('utf-8')
    )
    password = raw_input(
        u'Input your password (default - previous value: {}): '.format(
            data['password']
        ).encode('utf-8')
    )

    if devel_mode:
        data['devel_mode'] = (devel_mode == 'y')

    if user_addr:
        data['user_addr'] = user_addr

    if username:
        data['username'] = username

    if password:
        data['password'] = password

    with open(CONFIG_PATH, 'w') as f:
        f.write(json.dumps(data))

    print 'Config saved.'


def read_config():
    data = {
        'devel_mode': False,
        'user_addr': None,
        'username': None,
        'password': None,
    }

    if os.path.isfile(CONFIG_PATH):
        with open(CONFIG_PATH, 'r') as f:
            data.update(json.loads(f.read()))

    return data


if __name__ == '__main__':
    prompt_configure()
