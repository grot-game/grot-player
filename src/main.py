import httplib
import json
import os
import random
import socket
import time

from wsgiref.simple_server import make_server

from .commands.configure import read_config

SERVER_ADDRESS = 'epb.bolt.stxnext.pl'


def application(environ, start_response):
    """ Player wsgi application """
    start_response('200 OK', [('content-type', 'application/json')])
    request_body_size = int(environ['CONTENT_LENGTH'])
    request_body = environ['wsgi.input'].read(request_body_size)
    data = json.loads(request_body.decode('utf-8'))
    arrows = {
        'up': '^',
        'down': 'v',
        'left': '<',
        'right': '>',
    }
    board = data['board']
    print '\n'.join(map(
        lambda col: ''.join(map(
            lambda f: arrows[f['direction']],
            col,
        )),
        board,
    ))
    result = dict(
        x=random.randint(0, len(board[0]) - 1),
        y=random.randint(0, len(board) - 1),
    )
    response = json.dumps(result)
    return [response.encode('utf-8')]


def run_server():
    """ Running wsgi server """
    host = '0.0.0.0'
    port = 8000
    print("Serving on http://%s:%s..." % (host, port))
    make_server(host, port, application).serve_forever()


def update_address():
    missing_config = get_missing_config()
    if missing_config:
        print 'Missing config keys: {} - run config.sh.'.format(
            ', '.join(missing_config)
        )
        raise SystemExit(0)

    config = read_config()

    conn = httplib.HTTPConnection(SERVER_ADDRESS, timeout=5)
    data = json.dumps({
        'username': config['username'],
        'password': config['password'],
        'address': config['user_addr'],
    })
    headers = {'Content-type': 'application/json'}
    try:
        conn.request('POST', '/update_address', data, headers)
    except socket.error:
        return False

    resp_code = conn.getresponse().status
    if resp_code == 403:
        print 'Invalid username or password.'

    return resp_code == 200


def get_missing_config():
    required_config = ('username', 'password', 'user_addr')
    current_config = [key for key, val in read_config().iteritems() if val]
    return list(set(required_config) - set(current_config))


if __name__ == '__main__':
    if not read_config()['devel_mode']:
        print 'Attempting to update the player address on the game server.'
        while not update_address():
            time.sleep(2)
        print 'Address updated successfully, awaiting move decision requests.'

    run_server()
